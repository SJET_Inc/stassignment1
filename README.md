# README #

Steven's Assignment #1 for INFO2300.

### How do I get set up? ###

Simply download the repo files and launch the 'index.html' file.
The application will launch on your prefered web browser.

### MIT License Decision ###

I chose the MIT license as it allows anyone to freely utilize my sourcecode for educational/business purposes.
I never want to deny someone the ability to learn something new or be unable to build something innovative/creative.

Fish